baseURL = "https://vepain.gitlab.io/"
title = "Victor Epain"

theme = ["hugo-embed-pdf-shortcode", "fuji"]
hasCJKLanguage = true
enableEmoji = true
enableRobotsTXT = true
disableKinds = ["taxonomy"]
ignoreErrors = ["error-disable-taxonomy"]

## Change this two to switch between different language
languageCode = "en"           # For RSS, view https://www.rssboard.org/rss-language-codes
defaultContentLanguage = "en" # For HTML page, now support: en, zh-hans, zh-hant, ja, nl, pl, it

summaryLength = 100       # Custom summary length, add <!--more--> in post file to custom split point
pagination.paperSize = 10

enableGitInfo = true # e.g. to use updated date in pages

[languages]
    [languages.en]
        languageName = "English"
        languageCode = "en"
        title = 'Victor Epain'
        weight = 1

    [languages.fr]
        languageName = "Français"
        languageCode = "fr"
        title = 'Victor Epain'
        weight = 2


[outputFormats]
    [outputFormats.SearchIndex]
        isPlainText = true
        notAlternative = true
        mediaType = "application/json"
        path = "/search/"

[outputs]
    home = ["HTML", "RSS", "SearchIndex"]

[permalinks]
    post = "/:section/:filename/" # Custom post links, e.g. "/:year/:month/:title/"

[params]
    author = "vepain"     # You can also set author in post front matter individually
    defaultTheme = "auto" # default theme when first visit (auto|dark|light)

    # Source URL of the website, will appear in the footer
    sourceURL = "https://gitlab.com/vepain/vepain.gitlab.io"

    # Word counter and read time indicator in post metadata
    showWordCounter = false
    showReadTime = false

    # License in the footer
    showLicenseInFooter = false

    # License at the end of each post
    showLicense = true
    showToc = true


    # Copyright
    copyrightStartYear = "2022"

    # Open Graph & Twitter Card variables
    # You can also set description and images in post front matter individually
    description = "Victor Epain's scientific website."
    og = "/img/Portrait_08.JPG"                        # This will use the image called ... in static/img folder

    # Posts shown in list, search etc.
    mainSections = ["esr", "posts", "software"]

    # Bangumi image chart id
    # bgmImageChart = "000000"

    # License
    license = "CC BY-NC-SA 4.0"
    licenseLink = "http://creativecommons.org/licenses/by-nc-sa/4.0/"

    # Comments
    # utterances, see: https://utteranc.es/
    # utterancesRepo = "*/*"
    # utterancesIssueTerm = "pathname"

    # Disqus, see: https://disqus.com/admin/install/platforms/universalcode/
    # disqusShortname = "*********"
    # Also use DisqusJS for accessing from Mainland China, see: https://github.com/SukkaW/DisqusJS
    # If you want to set multiple api key, see theme's README for more details
    # disqusJSApi = "https://*********/"
    # disqusJSApikey = "**********"

    # custom lazyload placeholder
    # 16:9
    lazyPlaceholder = "/assets/lazyload/dsrca_loading_480x270.svg"
    # 32:9
    lazyPlaceholderRow = "/assets/lazyload/dsrca_loading_960x270.svg"
    # 8:9
    lazyPlaceholderCol = "/assets/lazyload/dsrca_loading_480x540.svg"

    # Let images display in full brightness under dark mode
    # disableDarkImage = true

[markup]
    [markup.goldmark]
        [markup.goldmark.renderer]
            unsafe = true # Enable user to embed HTML snippets in Markdown content
[markup.highlight]
    codeFences = false # Disable Hugo's code highlighter

[markup.tableOfContents]
    startLevel = 1
    endLevel = 3

[taxonomies]
    tag = "tags"

[build]
    noJSConfigInAssets = true

[security]
    enableInlineShortcodes = true
