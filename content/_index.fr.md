---
title: "Bienvenue !"
description: "Accueil"
date: 2022-06-01
showLicense: false
showToc: true
layout: homepage
---

<!-- LTeX: language=fr -->

***~ Bienvenue sur mon site scientifique !***

<img src="/img/Portrait_08.JPG" alt="Avatar" height="200">

Je suis actuellement chercheur postdoctoral en biologie computationnelle dans [l'équipe de recherche AlgoLab à l'Université de Milan-Bicocca](https://algolab.eu/) travaillant en pangénomique et en métagénomique.

***Mots clés :** algorithmique des graphes --- programmation mathématique --- assemblage de génomes --- métagénomique --- pangénomique.*

La traduction de mon site en français est en cours, veuillez vous référer à la version en anglais [https://vepain.gitlab.io/en/](https://vepain.gitlab.io/en/)
