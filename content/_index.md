---
title: "Home"
description: "vepain website homepage"
date: 2022-06-01
showLicense: false
showToc: true
layout: homepage
---

***~ Welcome to my scientific website!***

<img src="/img/Portrait_08.JPG" alt="Avatar" height="200">

I am currently a postdoc researcher in computational biology in the [AlgoLab research group at the University of Milano-Bicocca](https://algolab.eu/) working on pangenomic and metagenomic.

***Topics:** graph algorithms --- mathematical programming --- genome assembly --- metagenomic --- pangenomic.*

More information on the [**About**](/about) page.

<!--more-->

## ✨ Highlighted pages

{{<page-box-preview "/posts/2024-03-21_almob" >}}

---

{{<page-box-preview "/posts/2023-11-27_thesis" >}}

---

{{<page-box-preview "/posts/2023-10-02_atelier_ethique_youtube" >}}

## 🗺️ Website map

> 🌐 The language of the contents is detailed by EN and FR for respectively the English and the French languages

🏡 [**Home**](/) This homepage

🖋️ [**Posts**](/posts) Some posts (EN/FR)

💻 [**Software**](/software) List of developed software (EN)

🔬 [**ESR**](/esr) Some hierarchised posts about the University and the Research world, especially in France (FR)

🛡️ [**Privacy**](/privacy) Some hierarchised posts about data privacy (FR)

👤 [**About**](/about) Who am I? (EN)

⌛ [**Archives**](/archives) All leaf pages of this website (EN/FR)

🔎 [**Search**](/search) Are you looking for something (in leaf pages)? (EN/FR)

> 🔖 You can also use the tags (see right menu, in EN) to help you find your way around the site.

## 🎹 Piano improvisation!

<iframe width="560" height="315" src="https://www.youtube.com/embed/tFe5CzcLNSw?si=xaBnJyH178BXcFMb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<!-- For mastodon official link -->
<a rel="me" href="https://social.sciences.re/@vepain"></a>
