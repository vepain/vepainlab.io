---
title: "Who am I?"
description: "Young researcher & teacher"
date: 2022-06-01
showLicense: false
toc_end_level: 2
math: true
image: "/img/IMG_20220323_164434.jpg"
tags:
- "teaching"
- "research"
translationKey: "about"
---


<img src="/img/IMG_20220323_164434.jpg" alt="Avatar" width="200" height="200">

I am currently a postdoc researcher in computational biology in the [AlgoLab research group at the University of Milano-Bicocca](https://algolab.eu/).

I did my PhD study in the [Inria Rennes lab'](https://www.inria.fr/en/inria-centre-rennes-university) (FR), at the [GenScale team](https://team.inria.fr/genscale/).

My works are about genome assembly and scaffolding.
For this purpose, I implement graph algorithms and graph structures, and I formalise these problems thanks to mathematical programming.

I am member of the [non-profit organization ALIA for academic freedom](https://liberte-academique.fr/) and the associate treasurer of the [Nicomaque association for the popularisation of doctoral science](https://www.nicomaque-bretagne.fr/).
I also play the piano (you can find [here some improvisations!](https://www.youtube.com/@professeurchep))

📨 **Professional e-mail address** `<first_name><last_name><at>disroot.org`

<!--more-->

✨ **NEW** ✨ [Check my activity report for assistant professor french qualification for CNU 27](/posts/2024-12-06_CNU27)

<!-- TODO: add article on graphs -->
<!-- TODO: add article on assembly -->
<!-- TODO: add article on linear programming -->

## 👤 Work colleagues

👤 **PhD Inria main supervisor** [Rumen Andonov](http://people.irisa.fr/Rumen.Andonov/)

👤 **PhD INRAe main supervisor** [Jean-François Gibrat](https://www.researchgate.net/profile/Jean-Francois-Gibrat)

👤 **PhD second supervisor** [Dominique Lavenier](https://lavenier.net/homepage/)

## 🔗 On the Internet

<img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" alt="ORCID iD icon" width="14">**ORCID** <https://orcid.org/0000-0003-0049-0954>

<img src="https://hal.science/assets/favicon/apple-touch-icon.png" alt="HAL icon" width="14">**CV HAL** [victor-epain](https://cv.hal.science/victor-epain)

<img src="https://social.sciences.re/packs/media/icons/favicon-16x16-c58fdef40ced38d582d5b8eed9d15c5a.png" alt="Mastodon social.sciences.re icon" width="14">**Mastodon** <a rel="me" href="https://social.sciences.re/@vepain" target="_blank">@vepain@sciences.re</a> *(to be preferred)*

<img src="https://upload.wikimedia.org/wikipedia/commons/7/7a/Bluesky_Logo.svg" alt="BlueSky icon" width="14">**BlueSky** [@victorepain.cpesr.fr](https://bsky.app/profile/victorepain.cpesr.fr)

<img src="https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png" alt="GitLab icon" width="14">**GitLab** [@vepain](https://gitlab.com/vepain)

<img src="https://github.com/fluidicon.png" alt="GitHub icon" width="14">**GitHub** [vepain](https://github.com/vepain)

🎹 **Piano** [@ProfesseurChep](https://www.youtube.com/@ProfesseurChep)

---

You can find my CV below partitioned into several categories.

---

## 🎓 Education

**2020 - 2023 | Computer science PhD applied to genomics, Inria centre at Rennes**

* DNA fragment assembly: graph structures and chloroplast genome
scaffolding – Comparative analysis, combinatorial optimisation
problem formulation and implementation

**2018 – 2020 | Bioinformatics Master's degree**

* University of Rennes
* Master’s degree, with the highest honour

**2017 – 2018 | 3rd year bachelor's degree in computer science**

* University of Rennes – ISTIC
* Bachelor’s degree with honours

**2015 – 2017 | Preparation for national competitive entrance exams to leading French “grandes écoles”, specialising in mathematics and PHYSICS**

* Lycée Dupuy de Lôme, LORIENT

## 👥 Research project and collaborations

**2021 - 2022 | PhD mobility at [Heinrich Heine University Düsseldorf, Germany](https://www.hhu.de/en/)**

* [ALBI team](https://www.cs.hhu.de/en/research-groups/algorithmic-bioinformatics), welcomed by [Gunnar Klau](https://www.researchgate.net/profile/Gunnar-Klau) - collaboration with [Sven Schrinner](https://www.researchgate.net/scientific-contributions/Sven-Schrinner-2170156834)
* 2 months

**2020 | Master's degree internship in bioinformatics - Inria centre at Rennes**

* Integer linear programming strategy for long reads de novo genome assembly
GenScale team, leaders: [Dominique Lavenier](http://lavenier.net/homepage/), [Pierre Peterlongo](http://people.rennes.inria.fr/Pierre.Peterlongo/)
* 6 months

**2019 | Master's degree internship in bioinformatics Los Alamos National Laboratory (LANL), NM and Inria centre at Rennes, FR**

* Overlap graph partitioning for de novo long read assembly
* 3 months research visit financed by the grant HipcoGen (High-Performance
Combinatorial Optimization for Computational Genomics), associated team Inria-LANL
  * (1 month) LANL, Information Science Group CCS-3, leader: [Hristo Djidjev](https://www.researchgate.net/profile/Hristo-Djidjev)
  * (2 months) Inria centre at Rennes, GenScale team, leader: [Dominique Lavenier](http://lavenier.net/homepage/)

**2018 | Bachelor's degree end internship, bioinformatic team Inria centre at Rennes**

* Real chloroplast data and metadata extraction
* GenScale team, leader: [Dominique Lavenier](http://lavenier.net/homepage/)
* 2 months

## 📰 Publications

**Legend :**

* `IJ` international journal with peer reviewing
* `NC` national conference with peer reviewing
* `AC` conference abstract
* `PP` preprint

`IJ` Epain, V., Andonov, R. Global exact optimisations for chloroplast structural haplotype scaffolding. *Algorithms Mol Biol* **19**, 5 (2024). <https://doi.org/10.1186/s13015-023-00243-1>

`NC` V. Epain, R. Andonov, D. Lavenier. Optimal Scaffolding for Chloroplasts' Inverted Repeats. *JOBIM2022*, Jul 2022, Rennes, France. [⟨hal-03625229⟩](https://inria.hal.science/hal-03625229)

`PP` V. Epain. Overlap Graph for Assembling and Scaffolding Algorithms: Paradigm Review and Implementation Proposals. 2022. [⟨hal-03815190v3⟩](https://inria.hal.science/hal-03815190v3) [preprint]

`AC` R. Andonov, V. Epain, D. Lavenier. Optimal de novo assemblies for chloroplast genomes based on inverted repeats patterns. BiATA 2021 - *4th International conference Bioinformatics: from Algorithms to Applications*, Jul 2021, St. Petersbourg, Russia. pp.1-2. [⟨hal-03534195⟩](https://inria.hal.science/hal-03534195)

## 🗣️ Talks

**2023 | [ROADEF2023](https://roadef2023.sciencesconf.org/) - national conference**

* [Linear Integer Programming Approaches for Chloroplast Genome Scaffolding](https://roadef2023.sciencesconf.org/434884)

**2022 | [ROADEF2022](https://roadef2022.sciencesconf.org/) - national conference**

* [Linear integer programming approach for chloroplast genome scaffolding](https://roadef2022.sciencesconf.org/378696)

**2022 | [JOBIM2022](https://jobim2022.sciencesconf.org/) - Francophone conference**

* [Optimal Scaffolding for Chloroplasts’ Inverted Repeats](https://jobim2022.sciencesconf.org/data/pages/JOBIM2022_proceedings_oral.pdf)

**2022 | [DSB2022](https://dsb-meeting.github.io/DSB2022/) - international workshop**

* [The reverse complement symmetry advantage of DNA fragments relationships for their storage in a directed graph](https://dsb-meeting.github.io/DSB2022/slides/victor_epain.pdf)

**2021 | [BiATA2021](https://biata2021.spbu.ru/) - international conference**

* [Optimal de novo assemblies for chloroplast genomes based on inverted repeats patterns](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-021-04475-z#Sec5)

**2021 | [SeqBIM2021 Lyon](https://seqbim.cnrs.fr/seqbim-2021/) - national workshop**

* [The advantage of DNA reads overlaps’ reverse complement symmetry for their storage in an oriented graph](https://seqbim.cnrs.fr/wp-content/uploads/2021/11/Actes_seqBIM_2021.pdf)

**2020 | [ROADEF2020](https://roadef2020.sciencesconf.org/) - national conference**

* [Assemblage de Novo de Longues Lectures Par Programmation Linéaire](https://easychair.org/publications/preprint/sHWS)

## 🏫 Teaching

**2021 | Co-supervisor of [Pauline Hamon-Giraud](https://www.linkedin.com/in/pauline-hamon-giraud-b313811b6/)**

* First master’s degree student in bioinformatic, University of Rennes
* 3 months

**2021 | Algorithmic Methods for Graph**

* Third year Bachelor (INFO & MIAGE)
* Exercises lectures: 40h

**2021 | Operational Research**

* First year Master (MIAGE)
* Practical tutorial: 12h

**2021 | Algorithmic for Bioinformatic**

* Second year Master (Bioinformatic)
* Lecture: 2h
* Exercises lectures: 4h
* Practical tutorial: 6h

**2020 | Algorithmic Methods for Graph**

* Third year Bachelor (INFO & MIAGE)
* Exercises lectures: 40h

**2020 | Operational Research**

* First year Master (MIAGE)
* Practical tutorial: 12h
    > Statements and organisations of the practical tutorials have been rewritten to use `PuLP` `Python` package. The new versions are currently being taught for M1 MIAGE/INFO and M2 Bioinformatic Operational Research classes.

## 🥼 Services

**2023 | Data Knowledge Management IRISA/Inria Department**

* Organisation of monthly department seminar
* Organisation of [departments days](https://www.irisa.fr/date/2023-02/dkm-days-journees-dkm-23-mars-2023-amphi) (1.5 days)
  * Round table about how we can participate in the laboratory's and university's activities (teaching, council etc.)
  * Round table about scientific publication system

**2023 | [ROADEF2023](https://roadef2023.sciencesconf.org/) - national conference**

* Co-chairman for bioinformatic session (Recherche Opérationnelle en Bio-Informatique)

**2023 | Treasurer in [Nicomaque](https://www.nicomaque-bretagne.fr/) PhD association**

**2023 | Organisation of internship oral training for the first and second Master's degrees students**

**2022 | Data Knowledge Management IRISA/Inria Department**

* Organisation of monthly department seminar
* Organisation of [departments days](https://www.irisa.fr/date/2022-02/dkm-days-2022/) (1.5 days)

**2022 | [ROADEF2022](https://roadef2022.sciencesconf.org/) - national conference**

* Co-chairman for bioinformatic session (Recherche Opérationnelle en Bio-Informatique)

**2022 | [JOBIM2022](https://jobim2022.sciencesconf.org/) Francophone conference**

* Referent organiser for social activity

**2022 | Organisation of internship oral training for the first and second Master's degrees students**

**2021 | Supervisor of [Pauline Hamon-Giraud](https://www.linkedin.com/in/pauline-hamon-giraud-b313811b6/) (first Master degree student in bioinformatic)**

**2021 | Organisation of internship oral training for the second Master's degrees students**

## 💬 Science popularisation

**2023 | [Organisation of a conference on ethics in academia](https://atelier-ethique-inria2023.gitlab.io/)**

* [The recorded event (FR)](https://youtu.be/M8S5sF4kQGA?si=sd_Ri0N1W8wzlHEc)
* Recognised as scientific integrity and ethics training by Bretagne's PhD college

**2022 | Organisation of PhD popularisation film festival [Sciences en Cour[t]s](https://sciences-en-courts.fr/)**

* [The recorded event (FR)](https://youtu.be/1cyK35QFm7E)
* Collaboration with [Espace des Sciences](https://www.espace-sciences.org/) for the *Fête de la Science*

**2021 | Team member in [Sciences en Cour[t]s](https://sciences-en-courts.fr/) PhD popularisation film festival**

* Making a 5min animated film: [*Cocktail de bio-informatique*](https://youtu.be/4OVSAdGyuzs) (FR)

<!-- TODO: add studies -->

## 🖥️ Skills

### Programming languages and affiliated

**C / C++ / Rust (in progress)**

* [revsymg: fragment graph package](https://gitlab.com/vepain/rustrevsymg/-/tree/develop?ref_type=heads)

**Python3 (advanced)**

* [khloraascaf: chloroplast genome scaffolder](https://khloraa-scaffolding.readthedocs.io/en/latest/)
* [revsymg: fragment graph package](https://pyrevsymg.readthedocs.io/en/latest/)

**BASH** (advanced) | **AMPL** (advanced) | **Java**

$\LaTeX$ (advanced) | **Markdown** | **Git**

### Languages

* **French** (native speaker)
* **English** (good working knowledge)
* **Spanish** (notions)

<!--
# TODO working paper about section

## 📑 Working papers

**2023 | [Global exact optimisations for chloroplast genome multimeric forms scaffolding](https://inria.hal.science/hal-04134429)**
* See [khloraascaf software page](/software/khloraascaf) for more information
* **Preprint:** [*Inverted Repeats Scaffolding for a Dedicated Chloroplast Genome Assembler*](https://hal.inria.fr/hal-03684406)
* **Preprint:** [*Integer Programming Approach for Nested Pairs Genome Scaffolding*](https://hal.inria.fr/hal-03613353)

**2022 | [Overlap Graph for Assembling and Scaffolding Algorithms: Paradigm Review and Implementation Proposals](https://hal.inria.fr/hal-03815190)**
* *N.b.: used as a try of a thesis chapter*
* See [revsymg software page](/software/revsymg) for more information
* **Preprint:** [*Overlap Graphs for Assembling and Scaffolding Algorithms: Paradigm Review and Implementation Proposals*](https://hal.inria.fr/hal-03878293) -->
