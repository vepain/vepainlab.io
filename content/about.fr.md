---
title: "Qui suis-je ?"
description: "Jeune chercheur et enseignant"
date: 2022-06-01
showLicense: false
showToc: true
image: "/img/IMG_20220323_164434.jpg"
tags:
- "teaching"
- "research"
translationKey: "about"
---

<!-- Ltex: language=fr -->

 <img src="/img/IMG_20220323_164434.jpg" alt="Avatar"
width="200" height="200">

Je suis un doctorant de 3<sup>ème</sup> année au [laboratoire Inria de l'Université de Rennes](https://www.inria.fr/en/inria-centre-rennes-university) (FR), dans [l'équipe GenScale](https://team.inria.fr/genscale/).

Je travaille actuellement sur l'assemblage de génome et du scaffolding.
Pour résoudre ces problématiques, j'implémente des algorithmes sur des structures de graphes, et je formalise ces problèmes grâce à la programmation linéaire.

<!--more-->
<!-- FIXME mettre à jour CV fr -->
