---
title: "Enseignement Supérieur et Recherche"
description: "Petit guide pour un ESR éthique"
date: 2023-01-04
showLicense: false
showToc: true
layout: single
tags:
- "research"
- "ethic"
- "publication"
---

<!-- Ltex: language=fr -->

Navigation dans l'enseignement supérieur et la recherche (ESR), en particulier française.

<!--more-->

## La publication scientifique

* 📰 [Éthique de la publication scientifique](/esr/publication)


## Éthique et déontologie

* 📰 [Zoom sur l'avis du COMETS 2022-12-13](/posts/2022-12-13_avis_comets)
* 👥 [GDR RO - GdT RO et Éthique](http://gdrro.lip6.fr/?q=node/227)
* 👥 [GDR IM - GT Scalp : motion pour des créneaux de discussions politiques](https://www.irif.fr/gt-scalp/journees-2022/motion-17-02-2023)
* 🦉 [RogueESR : Liberté et éthique académiques](https://rogueesr.fr/20211027/)
* 📽️ [Conférence éthique dans l'Enseignement Supérieur et la Recherche, Enka Blanchard et Bernard Friot](https://www.youtube.com/watch?v=M8S5sF4kQGA&list=PLsrAkSTLO6qlFHBKDtzqNEj09TVE4qm3j)


## Quelques liens

* 🌻 [academia.hypotheses.org](https://academia.hypotheses.org/)
* 🔭 [Blog Julien Gossa (veille ESR, FR)](https://blog.educpros.fr/julien-gossa/)


## Abréviations

* **ESR** Enseignement Supérieur et Recherche
* **GDR** Groupements de Recherche CNRS
* **GT ou GdT** Groupe de Travail au sein d'un GDR
* **COMETS** COMité d'ÉThique Scientifique du CNRS : https://comite-ethique.cnrs.fr/
* **RO** Recherche Opérationnelle
* **IM** Informatique et Mathématique