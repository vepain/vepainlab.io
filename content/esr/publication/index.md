---
title: "Éthique de la publication en science"
description: "À propos des publications scientifiques"
date: 2023-01-04
showLicense: false
showToc: true
tags:
- "research"
- "ethic"
- "publication"
---

<!-- Ltex: language=fr -->

Cette page donne des éléments de réponse aux questions ci-dessous :

* Comment rendre l'accès ouvert à la recherche ?
* Quelle éthique pour la publication ?
* Quelles sont les recommandations collégiales et institutionnelles ?

<!--more-->

## Recommandations nationales

* 📖 [Ouvrir la Science - site ministériel](https://www.ouvrirlascience.fr/mettre-en-oeuvre-la-strategie-de-non-cession-des-droits-sur-les-publications-scientifiques/)
* 🪙 [Le CNRS encourage ses scientifiques à ne plus payer pour être publiés](https://www.cnrs.fr/fr/cnrsinfo/le-cnrs-encourage-ses-scientifiques-ne-plus-payer-pour-etre-publies)
* ©️ [Le CNRS demande désormais à ses chercheurs et chercheuses d’appliquer la stratégie de non-cession des droits d’auteur lors du dépôt de leurs articles auprès d’éditeurs.](https://www.cnrs.fr/fr/cnrsinfo/il-ny-pas-de-raison-que-les-scientifiques-fassent-une-cession-exclusive-gratuite-de-leurs)

## Recommandations locales

* 📖 [Science ouverte à l'Université de Rennes](https://scienceouverte.univ-rennes.fr/)


## Alternatives aux grands éditeurs

* 📚 [HAL - archives ouvertes](https://hal.archives-ouvertes.fr/)
* 🌳 [Le centre Mersenne](https://www.centre-mersenne.org/)
* 🗞️ [Peer Community In (PCI)](https://peercommunityin.org/)
    * 🖋️ [Comment PCI veut affranchir vos publis (metanews)](https://themeta.news/pci-va-t-il-renverser-lordre-etabli/)


## Manifestes, tribunes et actions

* 📜 [Manifeste de la guérilla pour le libre accès](https://framablog.org/2013/01/14/manifeste-guerilla-libre-acces-aaron-swartz/)
* ✊ [No free view? No review!](https://nofreeviewnoreview.org/)
* ✊ [The cost of knowledge](http://thecostofknowledge.com/)


<!-- ## D'autres liens

#FIXME attendre accord de Marie * [Site de Marie Farge sur la science ouverte](http://openscience.ens.fr/)
-->