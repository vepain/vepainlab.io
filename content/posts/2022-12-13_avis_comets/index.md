---
title: "Zoom sur l'Avis du COMETS"
description: "Opinion sur l'avis du comité d'éthique du CNRS"
date: 2022-12-13
showLicense: false
showToc: true
tags:
- "research"
- "ethic"
image: "/posts/2022-12-13_avis_comets/2022-12-13_comets.png"
---

<!-- Ltex: language=fr -->

<img src="/posts/2022-12-13_avis_comets/2022-12-13_comets.png"
 alt="Le fond de l'image représente un trou en forme d'avion dans une forêt, une loupe et le logo du CNRS sont en premier plan">

📰 Ce lundi 12 décembre 2022 est sorti un [nouvel avis](https://comite-ethique.cnrs.fr/avis-du-comets-integrer-les-enjeux-environnementaux-a-la-conduite-de-la-recherche-une-responsabilite-ethique/) du comité d'éthique du CNRS, *COMETS*.
Titré *« Intégrer les enjeux environnementaux à la conduite de la recherche – Une responsabilité éthique »*, il fait suite à une saisie du PDG du CNRS, [Antoine Petit](https://www.cnrs.fr/fr/antoine-petit-est-nomme-president-directeur-general-du-cnrs).

🔎 📃 Bien que l'avis complet d'une trentaine de pages soit disponible à la page du résumé, je me contente ici de mettre en exergue certains passages de ce dernier.

<!--more-->

## Points du résumé mis en exergue

### Une conclusion fondamentale en éthique

* C'est aux travailleur·euses de l'ESR d'être en responsabilité quant à la démarche éthique de leurs travaux.
* Iels ne peuvent l'être qu'à condition d'être en pleine capacité des moyens sur leur travail
* Une recherche tournée vers l'humain, plus que vers l'injonction à « *l'excellence* » ou « *l'innovation* »

### Quelle innovation ? Pour quoi faire ?

> *« Le monde de la recherche doit ainsi se demander dans quelle mesure le fait d’utiliser ou de développer tel grand équipement (jumeau numérique, accélérateur de particules, grand calculateur) ou de travailler sur telle thématique (biologie synthétique, édition du génome des plantes) est susceptible d’engendrer des impacts néfastes pour la biosphère, de conforter à moyen ou long terme des modes de production ou de consommation non durables, etc. »*

### Une redirection de l'idéologie politico-économique pour l'application de nos qualifications

> *« S’il convient de se garder de trop compter sur le développement de technologies de rupture dans un horizon de temps pertinent, il est nécessaire d’orienter davantage la recherche vers la poursuite de connaissances et de solutions favorables aux transformations de la société »*

### Décision collégiale, commune, communiste - appelez ça comme vous voulez

> *« C’est au monde de la recherche lui-même d’ouvrir en son sein un large débat sur ces questions. Pour le COMETS, il s’agit là d’une exigence première, bien avant toute mise en place d’instances ou de critères d’« évaluation environnementale » des projets de recherche, lesquels sont loin d’être inutiles mais pourraient contribuer à routiniser un questionnement qui exige avant toute chose d’être constitué en réflexion collective approfondie »*


## Pour aller plus loin

* 📗 Hélène Tordjman, La Croissance Verte Contre La Nature : Critique de l’écologie Marchande (Paris : La Découverte, 2021).
* 🦉 [RogueESR : Liberté et éthique académiques](https://rogueesr.fr/20211027/)
* 🌐 [Labos 1point5](https://labos1point5.org/)