---
title: "Réponse de mail formaté contre l'utilisation de mes données"
description: "Mail formaté à valeur juridique (EU)"
date: 2023-03-09
showLicense: false
showToc: true
tags:
- "privacy"
- "publication"
image: https://upload.wikimedia.org/wikipedia/commons/6/64/CNIL_logo_2016.svg
---

<!-- Ltex: language=fr -->

<img src="https://upload.wikimedia.org/wikipedia/commons/6/64/CNIL_logo_2016.svg"
 alt=" Logo de la Commission nationale de l'informatique et des libertés (CNIL) depuis 2016."
 width=500px>


📧 Vous recevez de la publicité numérique nominative comme :
* demande de relectures de journaux qui ne sont pas de votre domaine et, ou prédateurs ;
* autres demandes (p. ex. « Êtes-vous la bonne personne pour discuter caméra de sécurité chez Inria ? »)

⚖️ Répondez juridiquement : ci suit un modèle de mail à valeur juridique (inspiré des [modèles de réponses fournis par la CNIL](https://www.cnil.fr/fr/modeles/courrier))


<!--more-->

<!-- Ltex: language=en -->

## Mail example

> I cannot respond positively because I am not a researcher in the domain your journal covers.
>
> This leads me to the following reflection: you use some databases that own my data to contact me.
> Indeed, you could not have known me otherwise since I am not at all in the field covered by your newspaper.
>
> According to Article 21.1 of the General Data Protection Regulation (GDPR) European law, I object to the processing of my data by your organisation.
>
> Therefore, please :
>
> * Delete my data from your files and notify my request to the organisations to which you have communicated it (Articles 17.1.c. and 19 of the GDPR);
> * if you are legally obliged to do so, indicate how long my data will be kept in your archives;
> * inform me of these elements as soon as possible and at the latest within one month of receipt of this letter (article 12.3 of the GDPR).
>
> If you do not reply within the time limit set or if your reply is incomplete, I will refer the matter to the Commission Nationale de l'Informatique et des Libertés (CNIL) with a complaint.
>
> Regards,
>