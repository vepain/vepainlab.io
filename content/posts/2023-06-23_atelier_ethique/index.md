---
title: "Invitation à l'atelier Éthique dans l'ESR"
description: "Le 4 juillet 2023, Centre Inria de l'Université de Rennes"
date: 2023-06-23
showLicense: false
showToc: true
tags:
- "research"
- "ethic"
image: "/posts/2023-06-23_atelier_ethique/affiche_portrait.png"
---

<!-- Ltex: language=fr -->

<img src="/posts/2023-06-23_atelier_ethique/affiche_portrait.png" alt="Affiche format portrait de l'atelier" width="400">


À l'initiative de l'association des doctorant·es de Bretagne Nicomaque, nous avons le plaisir de vous inviter à l'Atelier Éthique dans l'Enseignement Supérieur et la Recherche qui se tiendra le 4 juillet, de 15h à 18h (en deux parties) au centre Inria de l'Université de Rennes.

Ouvert à tou·te·s, la participation à cet atelier est reconnu comme formation à l'éthique et à l'intégrité scientifique par le Collège doctoral de Bretagne (inscription pour les doctorant·es exclusivement sur Amethis).
Les non doctorant·es peuvent s'inscrire en présence sur le lien billet web.
Sinon, vous pouvez participer à la visioconférence.

Les liens et le détail de l'évènement se trouvent sur le site : https://atelier-ethique-inria2023.gitlab.io/

🗓️ Le 04 juillet 2023

⌚️ 15:00 à 16:30 (conférence), 17:00 à 18:00 (débat)

🗺️ Centre Inria de l’Université de Rennes

<!--more-->
