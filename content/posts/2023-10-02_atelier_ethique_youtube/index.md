---
title: "Publication vidéo de l'atelier Éthique dans l'ESR"
description: "Le 4 juillet 2023, Centre Inria de l'Université de Rennes"
date: 2023-10-02
showLicense: false
showToc: true
tags:
- "research"
- "ethic"
image: "/posts/2023-10-02_atelier_ethique_youtube/toustes.jpg"
---

<!-- Ltex: language=fr -->

📽️ Les retranscriptions de la conférence et de la partie débat/question sont maintenant disponibles sur [YouTube](https://www.youtube.com/watch?v=M8S5sF4kQGA&list=PLsrAkSTLO6qlFHBKDtzqNEj09TVE4qm3j) !

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=FulRbc9tawrlFEFN&amp;list=PLsrAkSTLO6qlFHBKDtzqNEj09TVE4qm3j" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<img src="/posts/2023-10-02_atelier_ethique_youtube/toustes.jpg" alt="Photo des intervenant·es et des animateur·ices" width="200">
<img src="/posts/2023-10-02_atelier_ethique_youtube/Enka_Ambre.png" alt="Photo d'Enka Blanchard et d'Ambre Ayats" width="200"> <img src="/posts/2023-10-02_atelier_ethique_youtube/Victor_Bernard.png" alt="Photo de Victor Epain et de Bernard Friot" width="200">


Le résumé de l'atelier, des ressources et d'autres informations sont disponibles sur le site web dédié : https://atelier-ethique-inria2023.gitlab.io/

<!--more-->

Merci de nouveau au [centre Inria de l'Université de Rennes](https://www.inria.fr/fr/centre-inria-universite-rennes) et à [l'Université de Rennes](https://www.univ-rennes.fr/) pour leur soutien matériel et financier.