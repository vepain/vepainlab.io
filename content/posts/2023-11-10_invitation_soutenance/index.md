---
title: "Invitation à ma soutenance de thèse"
description: "Le 27 novembre 2023 14:30, Centre Inria de l'Université de Rennes"
date: 2023-11-10
showLicense: false
showToc: true
tags:
- "research"
image: "/posts/2023-11-10_invitation_soutenance/2023-11-10_poster.png"
---

<!-- Ltex: language=fr -->


<img src="/posts/2023-11-10_invitation_soutenance/2023-11-10_poster.png"
 alt="Affiche de thèse avec références humoristiques"
 width="400">



J'ai le plaisir de vous inviter à ma soutenance de thèse qui se tiendra le :
* 🗓️ lundi 27 novembre 2023 ;
* ⌚️ 14:30 ;
* 🗺️ [Centre Inria de l'Université de Rennes](https://www.inria.fr/fr/centre-inria-universite-rennes), salle Métivier.
* 🎥 Lien visioconférence : https://inria.webex.com/meet/victor.epain

<!--more-->

À l'issu, il y aura :
* ☕ un pot ;
* 🧂 un restaurant le soir (crèpes à La Hublais, Cesson-Sévigné) ;
* 🏠 une soirée à l'appartement (oui, un lundi soir).

Pour m'aider à préparer cet évènement, merci de répondre aux questions ici : https://framadate.org/BDbx9KadeTTpb1hO (Merci de bien vouloir mettre un nom que je puisse identifier s'il vous plait.)


La présentation sera dans la langue de Molière (si tant est qu'il faisait de l'assemblage de génomes lui aussi) et le diapo dans la langue (simplifiée) de Shakespeare.


***Aux personnes extérieurs :** l’accès à cette soutenance est contraint à une inscription préalable obligatoire auprès de `victor [point] epain [arobase] inria [point] fr` – L’accès ne sera pas autorisé sans inscription préalable. Par ailleurs, les visiteurs ne porteront ni bagage ni sac (les sacs peuvent être déposés chez moi, 20min à pieds).*

**Titre :** Assemblage de fragments ADN : structures de graphes et échafaudage de génomes de chloroplastes

**Résumé :**

L'obtention de la séquence nucléotidique d'une molécule ADN nécessite sa fragmentation par des technologies de séquençage et l'assemblage des fragments. Ces fragments sont appelés lectures. Elles souffrent d'erreurs de séquençage et sont considérées sous deux orientations : celle de leur brin ADN d'origine ou l'inverse-complémentaire pour l'autre brin. L'assemblage se base sur des chevauchements deux à deux entre des lectures orientées, et est composé de trois phases : l'assemblage des lectures pour obtenir des contigs (des séquences plus longues que les lectures), l'échafaudage des contigs, pour obtenir des échafaudages (des ordres de contigs orientés), et la complétion des échafaudages (trouver les séquences de nucléotides séparant les contigs orientés dans les échafaudages).

Dans cette thèse, nous comparons des structures de graphes représentant des relations de successions entre des séquences ADN orientées, utiles à différentes phases de l'assemblage. Puis, nous nous penchons sur le problème de l'échafaudage dédié aux génomes de chloroplastes en proposant une nouvelle formulation, une résolution exacte et une implémentation.

**Rapporteur·ices :**

* Eric ANGEL, Professeur des universités, Université Paris-Saclay / Université d'Evry
* Annie CHATEAU, Maitresse de conférence, Université de Montpellier

**Jury :**

* Eric ANGEL, Professeur des universités, Université Paris-Saclay / Université d'Evry
* Annie CHATEAU, Maitresse de conférence, Université de Montpellier
* Elisa FROMONT, Professeure des universités, Université de Rennes
* Camille MARCHET, Chargée de recherche, CNRS CRIStAL, Lille
* Mathias WELLER, Professor, Institut für Softwaretechnik und Theoretische Informatik, Berlin

**Directeurs de thèse :**

* Rumen Andonov, Professeur des universités, Université de Rennes
* Jean-François GIBRAT, Directeur de recherche, INRAe, Île-de-France - Jouy-en-Josas - Antony

**Encadrant :**

* Dominique LAVENIER, Directeur de recherche, CNRS IRISA, Rennes
