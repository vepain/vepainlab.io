---
title: Dossier de qualification CNU section 27
date: 2024-12-06
showToc: false
image: "/posts/2024-12-06_cnu27/2024-12-06_CNU27.png"
tags:
  - research
  - teaching
---

<!-- LTeX: language=fr -->

Campagne de qualification pour MCF 2024-2025 par le CNU section 27 (informatique) : <https://cnu27.ls2n.fr/qualification/>

<!--more-->

{{< embed-pdf url="/doc/2024-12-06_cnu27_vepain.pdf" >}}
