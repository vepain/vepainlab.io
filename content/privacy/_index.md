---
title: "Confidentialité"
description: "Quelques éléments sur la confidentialité numérique"
date: 2023-03-09
showLicense: false
showToc: true
layout: single
tags:
- "privacy"
math: true
---

<!-- Ltex: language=fr -->

## Liens utiles

* 🛡️ [Le site de la CNIL](https://www.cnil.fr)
* $\pi$ [La Quadrature du Net](https://www.laquadrature.net/)

## Réflexes

* 📧 Répondre à celleux qui manipulent vos données : [exemple de mail](/posts/2023-03-09_personal_data_mail)