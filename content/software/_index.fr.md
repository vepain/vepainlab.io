---
title: "Logiciels"
description: "Liste de logiciels développés"
date: 2022-12-07
showLicense: false
showToc: true
tags:
- "research"
- "software"
---

Ci-suit la liste des logiciels dans lesquels je suis impliqué.

<!--more-->