---
title: "Khloraa Scaffolding"
description: "Python packages for chloroplast genome scaffolding."
date: 2023-06-04
showLicense: false
showToc: true
image: "/software/khloraascaf/logo_transp.png"
tags:
- "research"
- "software"
---

<img src="/software/khloraascaf/logo_transp.png"
 width=300
 alt="A directed graph representing links between oriented contigs. A red path shows the scaffolding result.">

Chloroplast genome scaffolding toolkit.

<!--more-->

## The scaffolder

* 🐍 **Python package** https://pypi.org/project/khloraascaf/
<!-- #TODO * 📑 **Preprint** [*Overlap Graph for Assembling and Scaffolding Algorithms: Paradigm Review and Implementation Proposals*](https://hal.inria.fr/hal-03815190) -->


## Utilities



<!-- #TODO khloraascaf_utils -->

## Reproducible benchmarks

<!-- #TODO khloraascaf_results -->

