---
title: "Graphe de fragments ADN"
description: "Graph data structure and some algorithms to store succession relationships between DNA fragments."
date: 2022-12-07
showLicense: false
showToc: true
image: "/software/revsymg/revsymg_logo_transp_crop.png"
tags:
- "research"
- "software"
---

<!-- FIXME French translation -->

<img src="/software/revsymg/revsymg_logo_transp_crop.png"
 width=400
 alt="A directed graph with several coloured parts.">

Graph data structure and some algorithms to store succession relationships between DNA fragments.

The main example is overlaps between DNA fragments: each overlap corresponds to one oriented DNA fragments that overlaps another oriented one, where the orientation can be the unchanged fragment's sequence or its reverse-complement.

<!--more-->

* 🐍 **Python package** https://pypi.org/project/revsymg/
* 🦀 **Rust package** *public in near future*
* 📑 **Preprint** [*Overlap Graph for Assembling and Scaffolding Algorithms: Paradigm Review and Implementation Proposals*](https://hal.inria.fr/hal-03815190)

